﻿#include "pch.h"
#include <iostream>
#include <string>
// Создаем класс Sheker 
class Sheker
{
public:
	Sheker(); // Конструктор 
	~Sheker(); // Деструктор
	const int size = 10; // Константа, размер массива
	int* mass; // Массив
	void Shekel(int *mass); // функция сортировки шейкером
	void bubl(int *mass); // функция сортировки пузырьком
	void setmass(int *mass, int size); // функция которая преписывает массив mass в поле datamass
	void printmas(); // выводит массив на консоль


private:
	int datamass[10]; // массив
};

// РЕАЛИЗАЦИЯ ФУНКЦИЙ КЛАССА
// конструктор (при создании объекта класса мы вызываем конструктор. Кнструктор Sheker заполняет массив datamass нулями
Sheker::Sheker()
{
	for (int i = 0; i < 10; i++) {
		datamass[i] = 0;
	}
}

// деструктор, уничтожает объект из памяти 
Sheker::~Sheker()
{

}
void Sheker::printmas()
{
	for (int i = 0; i < 10; i++) {
		std::cout << datamass[i] << " ";
	}
	std::cout << std::endl;
}

// пузырьком
void Sheker::bubl(int *mass) {
	int cache;
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 10 - i - 1; j++) {
			if (mass[j] > mass[j + 1]) {
				cache = mass[j];
				mass[j] = mass[j + 1];
				mass[j + 1] = cache;
			}
		}
	}
	for (int i = 0; i < 10; i++) {
		std::cout << mass[i] << " ";
	}
	std::cout << std::endl;
}

// шейкером
void Sheker::Shekel(int *mass)
{

	int low = 1;
	int high = 9;
	int cachee;
	while (low < high)
	{
		for (int i = 0; i > 10; i++) {
			if (mass[i] > mass[i + 1]) {
				cachee = mass[i];
				mass[i] = mass[i + 1];
				mass[i + 1] = cachee;
			}
			high = high + 1;
		}
		for (int j = 0; j < 10; j++) {
			if (mass[j] < mass[j - 1]) {
				cachee = mass[j];
				mass[j] = mass[j - 1];
				mass[j - 1] = cachee;
			}
			low = low - 1;
		}

	}
	for (int i = 0; i < 10; i++) {
		std::cout << mass[i] << " ";
	}
	std::cout << std::endl;


}

// функция которая преписывает массив mass в поле datamass
void Sheker::setmass(int *mass, int size)
{
	for (int i = 0; i < size; i++) {
		datamass[i] = mass[i];
	}
}


//class Student //Класс- описание сложного типа данных, конструкция языка с++
//	// процедура или функция входщая в состав - метод, переменная входящая в состав класска-свойство, 
//	//
//{
//public: // доступный 
//	Student(); // конструктр создает элемент класса 
//	~Student();// удаляет наш класс из памяти диструктор
//
//	Student(int st_group, int st_age);
//
//	void set_group(int st_group); //описание метода 
//	int get_group();
//	int age;
//	void set_surname(char* st_surname, int st_surnamesize);
//	char* get_surname();
//private:// закрытые данные
//	char surname[20];
//	int group;
//
//
//protected:
//
//
//};
//5 структуры 
// Не смотря на то что в литературе структуры как правило хранит только данные, на самом деле это 
//то же самое, что и класс. Единственное отличие - поля по умолчанию public.
// Автоматизм конструктора и диструктора  обеспечивается высокоуровневыми стредствами автоматизации языка.
// к строке где обявлен объект, компилятор не явно от программиста дописывает вызов класса,
// Инкапсуляция = фактически объединение данных и методов, объединеных едином смыслом,
//в одну конструкцию языка.
// Полиморфизм - простыми словами: экземпляры одного класса ведут себя по разному.
// наследование - создание производных классов от родительского и заимствование его свойств и методов.
// Инкапсуляция, наследование, полиморфизм - три основные идеи ООП.

// До этого были "инструкции" (описание классов, их функций и тд). А теперь main
int main()
{
	int mass1[] = { 11,-9,-6,3,8,1,0,2,-9,11 };
	int mass2[] = { 11,-9,-6,3,8,1,0,2,-9,11 };

	// создаем объект класса Sheker с именем Mass1
	Sheker Mass1;

	// вызываем ффункцию  setmass к объекту Mass1
	Mass1.setmass(mass1, 10);
	// вызываем ффункцию  printmas к объекту Mass1
	Mass1.printmas();
	// вызываем ффункцию  Shekel к объекту Mass1
	Mass1.Shekel(mass1);

	// создаем объект класса Sheker с именем Mass2 и делаемто же самое только пузырьком
	Sheker Mass2;
	Mass2.setmass(mass2, 10);
	Mass2.printmas();
	Mass2.bubl(mass2);

	return 0;
}
