﻿#include "pch.h"
#include <iostream>

class matrix
{
private:

	double matr[10][10];// написали много, потому что с запасом
	double p_matr[10][10];
	int rows = 10;
	int columns = 10;
	int p_rows = 10;
	int p_columns = 10;


public:
	matrix(); //констурктор
	~matrix(); //деструкор

	void setMatrix(); // сеттер для матрицы

	bool input(double matr[10][10]); //ф-я ввода

	int ret();

	int inputsize();

	int getelem();

	void print(); // ф-я вывода

	bool summMatrix(matrix matr2); //ф-я суммы
	bool multMatrix(matrix matr2); //ф-я произведения
	bool transp(); // ф-я транспонирования

	/*double getElem(int row, int col)
	{
		if (row<rows && col<columns && row>-1 && col>-1)
		{
			return matr[row][col];
		}
		std::cout << "Connot get elenent. Index Error!\n";
		return	-1;
	}*/
};

int main()
{
	setlocale(LC_ALL, "Russian");
	double data1[10][10];
	double data2[10][10];

	matrix matrA, matrB;
	std::cout << "Введите размеры для матрицы A \n";
	matrA.inputsize();
	matrA.setMatrix();
	matrA.getelem();

	std::cout << "\n";
	std::cout << "Введите размеры для матрицы B\n";
	matrB.inputsize();
	matrB.setMatrix();

	std::cout << "\n\n------------------------------------------------------------------\n\n";
	std::cout << "Матрица А \n\n";
	matrA.print();
	std::cout << "\n";
	std::cout << "Матрица Б \n\n";
	matrB.print();

	//складываем матрицы
	matrA.summMatrix(matrB);
	matrA.ret();
	//умножаем матрицы
	matrA.multMatrix(matrB);
	//транспонируем матрицы
	matrA.transp();
	matrA.print();
	matrA.ret();

	getchar();
	return 0;
}

matrix::matrix()
{
}

matrix::~matrix()
{
}

int matrix::inputsize()
{
	std::cout << "Введите кол-во строк: ";
	std::cin >> rows;
	std::cout << "Введите кол-во столбцов: ";
	std::cin >> columns;
	return 0;
}

int matrix::getelem()
{
	p_rows = rows;
	p_columns = columns;
	for (int i = 0; i < p_rows; i++)
		for (int j = 0; j < p_columns; j++)
		{
			p_matr[i][j] = matr[i][j];
		}
	return 0;
}


void matrix::setMatrix()
{
	std::cout << "\n";

	for (int i = 0; i < rows; i++)
		for (int j = 0; j < columns; j++)
		{
			matr[i][j] = rand() % 5;
		}
}


void matrix::print()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			std::cout << matr[i][j] << "\t";
		}
		std::cout << "\n";
	}
}

bool matrix::summMatrix(matrix matrB)
{
	if (rows == matrB.rows & columns == matrB.columns)
	{
		std::cout << "\n\nСумма матриц равна\n\n";

		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				matr[i][j] += matrB.matr[i][j];
			}
		}
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < columns; j++)
			{
				std::cout << matr[i][j] << "\t";
			}
			std::cout << "\n";
		}
		return true;
	}
	else {
		std::cout << "\n\nОшибка в сумме\n";
		return false;
	}

}

bool matrix::multMatrix(matrix  matrB)
{


	if (columns == matrB.rows)
	{
		double **c = new double*[rows];
		c = new double*[rows];
		for (int i = 0; i < rows; i++)
		{
			c[i] = new double[matrB.columns];
			for (int j = 0; j < matrB.columns; j++)
			{
				c[i][j] = 0;
				for (int k = 0; k < columns; k++)
					c[i][j] += matr[i][k] * matrB.matr[k][j];
			}
		}

		std::cout << "\n\nМатрица произведения\n" << std::endl;
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < matrB.columns; j++)
				std::cout << c[i][j] << "\t";
			std::cout << std::endl;
		}
		return true;
	}

	else
	{
		std::cout << "\nОшибка в произведении\n";
	}
	return false;
}

bool matrix::input(double matr[10][10])
{
	int getrows;
	std::cin >> getrows;//считать количество строк
	if (getrows < 1 || getrows > 10)
	{
		return false;
	}
	else
	{
		return true;
	}
	int getcolums;
	std::cin >> getcolums;
	if (getcolums < 1 || getcolums > 10)
	{
		return false;
	}
	else
	{
		return true;
	}
	for (int a = 0; a < getrows; a++)
		for (int b = 0; b < getcolums; b++)
		{
			std::cin >> matr[getrows][getcolums];
		}
	return true;
}

int matrix::ret()
{
	rows = p_rows;
	columns = p_columns;
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr[i][j] = p_matr[i][j];
		}
	}
	return 0;
}



bool matrix::transp()
{
	std::cout << "\nTранспонированная матрица A: \n\n";
	int arr1[10][10];
	int t = rows;
	rows = columns;
	columns = t;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			arr1[i][j] = matr[j][i];
		}
	}
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < columns; j++) {
			matr[i][j] = arr1[i][j];
		}
	}
	return true;
}

