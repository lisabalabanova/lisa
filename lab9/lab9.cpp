﻿#include "pch.h"
#include <iostream>

class Matrix
{
private:
	int* a;
	int m = 10;
	int n = 10;
public:
	Matrix();
	//операторы перегрузки
	friend Matrix operator + (Matrix &left, Matrix &right);
	friend Matrix operator - (Matrix &left, Matrix &right);
	friend std::ostream &operator << (std::ostream &os, Matrix &matr);
	Matrix operator = (Matrix &matr);



	void print();
	void setMatrix(int* new_a, int m1, int n1);
	void input();
	void trans();
	void sum_matrix(Matrix b);
	void multi_matrix(Matrix b);
	int get_m() { return m; }
	int get_n() { return n; }
	double getElem(int row, int col)
	{
		if (row<m && col<n && row>-1 && col>-1)
		{
			return a[row*n + col];
		}
		std::cout << "Connot get elenent. Index Error!\n";
		return -1;
	};
	bool setElem(int row, int col, double elem) {
		if (row<m && col<n && row>-1 && col>-1)
		{
			a[row*n + col] = elem;
			return true;
		}
		std::cout << "Connot set elenent. Index Error!\n";
		return false;
	};
};

std::ostream& operator << (std::ostream &os, Matrix &matr)
{
	for (int i = 0; i < matr.get_m(); i++) {
		for (int j = 0; j < matr.get_n(); j++) {
			os << matr.getElem(i, j) << "\t";
		};
		os << std::endl;
	};
	return os;
}

Matrix Matrix::operator = (Matrix &matr)
{
	return matr;
}

Matrix operator + (Matrix &left, Matrix &right)
{
	if ((left.get_m() == right.get_m()) && (left.get_n() == right.get_n())) {
		int * new_a = new int[left.get_m()*right.get_n()];
		for (int i = 0; i < left.get_m(); i++) {
			for (int j = 0; j < left.get_n(); j++) {
				new_a[i*left.get_n() + j] = left.getElem(i, j) + right.getElem(i, j);
			};
		};
		Matrix matr;
		matr.setMatrix(new_a, left.get_m(), left.get_n());
		return matr;
	}
	Matrix matr2;
	return matr2;
}

Matrix operator - (Matrix &left, Matrix &right)
{
	if ((left.get_m() == right.get_m()) && (left.get_n() == right.get_n())) {
		int * new_a = new int[left.get_m()*right.get_n()];
		for (int i = 0; i < left.get_m(); i++) {
			for (int j = 0; j < left.get_n(); j++) {
				new_a[i*left.get_n() + j] = left.getElem(i, j) - right.getElem(i, j);
			};
		};
		Matrix matr;
		matr.setMatrix(new_a, left.get_m(), left.get_n());
		return matr;
	}
	Matrix matr2;
	return matr2;
}

Matrix::Matrix() {
	delete[] a;
	int num = 255;
	a = new int[num];
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			a[i*n + j] = 0;
		}
	}
}

void Matrix::trans() {
	int * new_a = new int[m*n];
	if (m < n) {
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < m; j++) {
				new_a[i*n + j] = a[i*n + j];
			};
		};
		for (int i = 0; i < m; i++) {
			for (int j = i + 1; j < m; j++) {
				int buf = new_a[j*n + i];
				new_a[j*n + i] = new_a[i*n + j];
				new_a[i*n + j] = buf;
			};
		};
		for (int i = 0; i < m; i++) {
			for (int j = n - m; j < n; j++) {
				new_a[j*m + i] = a[i*n + j];
			}
		};
		int buf = m;
		m = n;
		n = buf;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				a[i*n + j] = new_a[i*n + j];
			};
		};

	}
	else if (n < m) {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				new_a[i*n + j] = a[i*n + j];
			};
		};
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				int buf = new_a[j*n + i];
				new_a[j*n + i] = new_a[i*n + j];
				new_a[i*n + j] = buf;
			};
		};
		for (int i = 0; i < m; i++) {
			for (int j = n - m; j < n; j++) {
				new_a[j*m + i] = a[i*n + j];
			}
		};
		int buf = m;
		m = n;
		n = buf;
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				a[i*n + j] = new_a[i*n + j];
			};
		};
	}
	else {
		for (int i = 0; i < n; i++) {
			for (int j = i + 1; j < n; j++) {
				int buf = a[j*n + i];
				a[j*n + i] = a[i*n + j];
				a[i*n + j] = buf;
			};
		};
	}

}

void Matrix::multi_matrix(Matrix b) {
	if (n == b.get_m()) {
		int * new_a = new int[b.get_n()*m];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < b.get_n(); j++) {
				new_a[i*b.get_n() + j] = 0;
			}
		}
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < b.get_n(); j++) {
				for (int k = 0; k < n; k++) {
					new_a[i*b.get_n() + j] += a[i*n + k] * b.getElem(k, j);
				};
			};
		};
		n = b.get_n();
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				a[i*n + j] = new_a[i*n + j];
			};
		};
		delete[] new_a;
	}
}

void Matrix::input()
{
	int new_rows;
	std::cin >> new_rows;
	int new_colums;
	std::cin >> new_colums;
	if (new_colums >= 1 && new_colums < 10 && new_rows >= 1 && new_rows < 10)
	{
		for (int i = 0; i < new_rows; i++) {
			for (int j = 0; j < new_colums; j++) {
				std::cin >> a[i*new_colums + j];
			}
		}
	}
	m = new_rows;
	n = new_colums;
}

void Matrix::sum_matrix(Matrix b) {
	if ((m == b.get_m()) && (n == b.get_n())) {
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				a[i*n + j] = a[i*n + j] + b.getElem(i, j);
			};
		};
	}
}

void Matrix::print() {
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			std::cout << a[i*n + j] << "\t";
		};
		std::cout << std::endl;
	};
}

void Matrix::setMatrix(int* new_a, int m1, int n1) {
	m = m1;
	n = n1;
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			a[i*n + j] = new_a[i*n + j];
		};
	};
}

class Vector : public Matrix
{
public:
	double scalMultVector(Vector vec2);
	double MultVector(int num);
};

double Vector::MultVector(int num) {
	for (int i = 0; i < this->get_n(); i++) {
		this->setElem(0, i, this->getElem(0, i) * num);
	}
	return 1;
}

double Vector::scalMultVector(Vector vec2) {
	if (this->get_n() != vec2.get_n()) {
		return 0;
	}
	for (int i = 0; i < vec2.get_n(); i++) {
		this->setElem(0, i, this->getElem(0, i) * vec2.getElem(0, i));
	}
	return 1;
}

int main() {
	//инициализируем матрицы
	Matrix matrA, matrB;
	std::cout << "vvedite razmer matrici a \n";
	matrA.input();
	matrA.print();
	std::cout << std::endl;
	matrA.trans();
	matrA.print();
	std::cout << std::endl;

	std::cout << "vvedite razmer matrici b \n";
	matrB.input();
	matrB.multi_matrix(matrA);
	matrB.print();
	std::cout << std::endl;
	matrB.sum_matrix(matrA);
	matrB.print();
	std::cout << "vvedi vector";
	Vector xA;
	xA.input();
	xA.print();
	xA.MultVector(5);
	std::cout << std::endl;
	xA.print();


	return 0;
	system("pause");
}