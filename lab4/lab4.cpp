﻿#include "pch.h"
#include <iostream>

// Функция сортировка пузырьком 
void bubble(int *arr, int size)
{
	for (int y = 0; y < size; y++)
	{
		for (int z = 0; z < size - 1; z++)
		{
			if (arr[z] > arr[z + 1])
			{
				int temp = arr[z];
				arr[z] = arr[z + 1];
				arr[z + 1] = temp;
			}
		}
	}
}

int main()
{
	std::cout << "lab4\n" << std::endl;
	std::cout << "Array in start: 8 -9 4 1 7 2\n" << std::endl;
	int array[6] = { 8, -9, 4, 1, 7, 2 };
	bubble(array, 6);
	std::cout << "Bubble-sorted: ";
	for (int i = 0; i < 6; i++)
		std::cout << array[i] << " ";
	std::cout << std::endl;
	getchar();
	return 0;
}
