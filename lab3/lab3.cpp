﻿#include <string>
#include "pch.h"
#include <iostream>

int main()
{
	setlocale(LC_ALL, "Rus");
	//Задание номер 1: Типы данных (выводим размер ОП и его диапазон).
	std::cout << "Size of char : " << sizeof(char) << "     " << (pow(2, sizeof(char) * 8.0) - 1) << std::endl;
	std::cout << "Size of int : " << sizeof(int) << "     " << (pow(2, sizeof(int) * 8.0 - 1) - 1) << std::endl;
	std::cout << "Size of short int : " << sizeof(short int) << "     " << (pow(2, sizeof(short int) * 8.0 - 1) - 1) << std::endl;
	std::cout << "Size of long int : " << sizeof(long int) << "     " << (pow(2, sizeof(unsigned long int) * 8.0) - 1) << std::endl;
	std::cout << "Size of float : " << sizeof(float) << "     " << (pow(2, sizeof(float) * 8.0 - 1) - 1) << std::endl;
	std::cout << "Size of double : " << sizeof(double) << "     " << (pow(2, sizeof(double) * 8.0 - 1) - 1) << std::endl;
	std::cout << "Size of bool : " << sizeof(bool) << "     " << (pow(2, sizeof(bool) * 8.0) - 1) << std::endl;

	//Задание номер 4: Шейкер-сортировка.
	int mass_size = 12;
	int mass[] = { 8,-13,16,-9,-5,0,3,-1,12,10,6,7 };
	int low, high;
	int cache;
	low = 0;
	high = mass_size - 1;
	while (low < high)
	{
		for (int i = 0; i > mass_size; i++) {
			if (mass[i] > mass[i + 1]) {
				cache = mass[i];
				mass[i] = mass[i + 1];
				mass[i + 1] = cache;
			}
			high = high + 1;
		}
		for (int j = 0; j < mass_size; j++) {
			if (mass[j] < mass[j - 1]) {
				cache = mass[j];
				mass[j] = mass[j - 1];
				mass[j - 1] = cache;
			}
			low = low - 1;
		}

	}
	for (int i = 0; i < mass_size; i++) {
		std::cout << mass[i] << " ";
	}
	std::cout << "\n";

	//Задание номер 3: Перебор массива пузырьком.

	int size = 6;
	int arr[] = {-6,-8,4,12,2,6 };
	for (int y = 0; y < size; y++)
	{
		for (int z = 0; z < size - 1; z++)
		{
			if (arr[z] > arr[z + 1])
			{
				int temp = arr[z];
				arr[z] = arr[z + 1];
				arr[z + 1] = temp;
			}
		}
	}
	for (int i = 0; i < size; i++) {
		std::cout << arr[i] << " ";
	}

	
	std::cout << "\n";
	// Задание номер 2: Шифр Цезаря.
	int k = 22;
	int n = 33;
	int s;
	char str[200] = { "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" };
	std::cout << "Строка до шифрования " << std::endl;
	std::cout << str << std::endl;
	for (int i = 0; i < strlen(str); i++) {

		str[i] = (str[i] + k) % (n + __toascii('А'));
	}
	std::cout << "Строка после шифрования " << std::endl;
	std::cout << str;
	for (int i = 0; i < strlen(str); i++) {

		str[i] = (str[i] - k - (n + __toascii('А'))) % (n + __toascii('А'));
	}

	return 0;

}